import React, { Component } from "react";
import NavMenu from "../nonUsedComponents/NavMenu";
import Member from "../nonUsedComponents/Member";

class Main extends Component {
  render() {
    return (
      <div>
        <NavMenu className="home-page-menu" />
        <Member
          admin={true}
          name="Edna Welch"
          email="edna.welch56@gmail.com"
          thumbnail="https://randomuser.me/api/portraits/women/71.jpg"
          makeAdmin={email => console.log(email)}
        />
      </div>
    );
  }
}

export default Main;
