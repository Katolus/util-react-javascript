import React, {Component} from "react";
import PropTypes from 'prop-types';

const autoCompletionData = [
    "Alpine Meadows", 
    "Boreal",
    "Diamond Peak",
    "Donner Ski",
    "Heavenly",
    "Homewood",
    "Kirkwood",
    "Mt. Rose",
    "Northstar",
    "Squaw Valley",
    "Sugar Bowl"
]

class Autocomplete extends Component {
    get value() {
        return this.refs.inputResort.value;
    }

    set value(inputResort) {
        this.refs.inputResort.value = inputResort;
    }

    render(){
        return (
            <div>
                <input ref="inputResort" type="text" list="autoCompletionData"/>
                <datalist id="autoCompletionData">
                    {this.props.options.map(
                        (opt,i) => <option key={i}>{opt}</option>
                    )}
                </datalist>
            </div>
        )
    }
}

export const AddDayFrom = ({ resort, date, powder, backcountry }) => {

    let _resort, _date, _powder, _backcountry;

    const submit = (e) => {
        e.preventDefault();
        // onSend({
        //     resort: _resort.value
        // });
        console.log('resort', _resort.value);
        console.log('date', _date.value);
        console.log('powder', _powder.checked);
        console.log('backcountry', _backcountry.checked);
    }

    return (
      <form onSubmit={submit} className="add-day-form">
        <label htmlFor="resort">Resort name</label>
        <Autocomplete options={autoCompletionData} ref={input => _resort = input}/>
        {/* <input id="resort" type="text" required defaultValue={resort} ref={input => _resort = input}/> */}
        <label htmlFor="date">Date</label>
        <input id="date" type="date" required defaultValue={date} ref={input => _date = input}/>
        <div>
          <input id="powder" type="checkbox" defaultChecked={powder} ref={input => _powder = input}/>
          <label htmlFor="powder">Powder Day</label>
        </div>
        <div>
          <input id="backcountry" type="checkbox" defaultChecked={backcountry} ref={input => _backcountry = input}/>
          <label htmlFor="backcountry">Backcountry Day</label>
        </div>
        <button>Add day</button>
      </form>
    );
}

AddDayFrom.defaultProps = {
    resort: "Kirkwood",
    date: "2018-02-01",
    powder: true,
    backcountry: true,
};

AddDayFrom.propTypes = {
  resort: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  powder: PropTypes.bool.isRequired,
  backcountry: PropTypes.bool.isRequired
};
