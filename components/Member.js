import FaShield from "react-icons/lib/fa/shield";
import React, { Component } from "react";
import PropTypes from 'prop-types';

export default class Member extends Component {
  constructor(props) {
    super(props);
    this.style = {
      background: "gray"
    };
  }

  UNSAFE_componentWillUpdate(nextProps) {
    this.style = {
      background: nextProps.admin ? "green" : "purple"
    };
  }
  // constructor(props){
  //     super(props)
  //     this.props = this.bind
  // }
  render() {
    const {
      admin,
      name,
      email,
      thumbnail,
      makeAdmin,
      removeAdmin
    } = this.props;
    return (
      <div className="member" style={this.style}>
        <h1>
          {name} {admin ? <FaShield /> : null}
        </h1>
        <p>
          {admin ? (
            <a onClick={() => removeAdmin(email)}>Remove Admin</a>
          ) : (
            <a onClick={() => makeAdmin(email)}>Make Admin</a>
          )}
        </p>
        <img src={thumbnail} alt="profile picture" />
        <p>
          <a href={`mailto:${email}?Subject=Welcome Edna`}>Send e-mail</a>
        </p>
      </div>
    );
  }
}

Member.propTypes = {
  admin: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired,
  makeAdmin: PropTypes.func.isRequired,
  removeAdmin: PropTypes.func.isRequired
}