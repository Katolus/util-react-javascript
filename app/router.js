import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { AddDayFrom } from "./components/AddDayFrom";
import MemberList from "./components/MemberList";

export default () => ( 
  <BrowserRouter>
    <Switch>
      <Route path="/test" render={props => <Test {...props} />} />
      <Route path="/form" component={AddDayFrom}/>
      <Route path="/list" component={MemberList}/>
    </Switch>
  </BrowserRouter>
);